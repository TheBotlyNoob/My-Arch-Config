# My Arch Config

This Is A Bash Script That Configures A Newly Installed Arch Based Distro With GNOME.

For Config, See [.env.example](./.env.example)

## Usage
To use, run
```bash
curl -sSL "https://j-j.vercel.app/config.sh" | sudo bash
```
