#!/usr/bin/zsh

# Use powerline
USE_POWERLINE="true"
# Source manjaro-zsh-configuration
if [[ -e /usr/share/zsh/manjaro-zsh-config ]]; then
  source /usr/share/zsh/manjaro-zsh-config
fi
# Use manjaro zsh prompt
if [[ -e /usr/share/zsh/manjaro-zsh-prompt ]]; then
  source /usr/share/zsh/manjaro-zsh-prompt
fi

#
# # ex - archive extractor
# # usage: ex <file>
ex ()
{
  if [ -f "$1" ] ; then
    case "$1" in
      *.tar.bz2)   tar xjf     "$1"    ;;
      *.tar.gz)    tar xzf     "$1"    ;;
      *.bz2)       bunzip2     "$1"    ;;
      *.rar)       unrar x     "$1"    ;;
      *.gz)        gunzip      "$1"    ;;
      *.tar)       tar xf      "$1"    ;;
      *.tbz2)      tar xjf     "$1"    ;;
      *.tgz)       tar xzf     "$1"    ;;
      *.zip)       unzip       "$1"    ;;
      *.Z)         uncompress  "$1"    ;;
      *.7z)        7z x        "$1"    ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

if [ -f ~/.zsh_aliases ]; then
  source ~/.zsh_aliases
fi

if [ -f /usr/share/zsh-theme-powerlevel10k/powerlevel10k.zsh-theme ]; then
  source /usr/share/zsh-theme-powerlevel10k/powerlevel10k.zsh-theme
fi

if [ -f /venvs/default/scripts/activate ]; then
  source /venvs/default/bin/activate
fi

## Set PATH Envvar

getPath() {
    for app in /apps/*; do
        make -C "$app" &>/dev/null
        if [ -d "$app/bin" ]; then
            path=("$app/bin" $path)
        elif [ -d "$app/dist" ]; then
                path=("$app/dist" $path)
        else path=("$app" $path)
        fi
    done
    echo "$PATH"
}

# https://stackoverflow.com/a/58598185/14334900
# capture the output of a command so it can be retrieved with ret
cap () { tee ~/.capture.out; }

# return the output of the most recent command that was captured by cap
ret () { cat ~/.capture.out; }

export PATH="$(getPath)"

export WINEDEBUG=-all

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
