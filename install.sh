#!/bin/bash

dns_servers=("1.1.1.1" "1.0.0.1" "8.8.8.8" "8.8.4.4")
pip_packages=("venv-run")
npm_packages=("playit.gg" "degit" "npm-check-updates" "http-server")
packages=("socat" "desktopfolder" "git" "base-devel" "netcat" "desktopfolder" "neofetch" "python-poetry" "networkmanager" "ffmpeg" "clang" "cmake" "manjaro-zsh-config" "virtualbox-ext-oracle" "virtualbox" "lutris" "google-chrome" "zoom" "bat" "lsd" "visual-studio-code-bin" "powershell-bin" "curl" "wget" "htop" "lynx" "minecraft-launcher" "mpv-git" "steam" "discord" "spotify" "obs-studio" "grapejuice-git" "nodejs" "npm" "wine-staging" "giflib" "lib32-giflib" "libpng" "lib32-libpng" "libldap" "lib32-libldap" "gnutls" "lib32-gnutls" "mpg123" "lib32-mpg123" "openal" "lib32-openal" "v4l-utils" "lib32-v4l-utils" "libpulse" "lib32-libpulse" "libgpg-error" "lib32-libgpg-error" "alsa-plugins" "lib32-alsa-plugins" "alsa-lib" "lib32-alsa-lib" "libjpeg-turbo" "lib32-libjpeg-turbo" "sqlite" "lib32-sqlite" "libxcomposite" "lib32-libxcomposite" "libxinerama" "lib32-libgcrypt" "libgcrypt" "lib32-libxinerama" "ncurses" "lib32-ncurses" "opencl-icd-loader" "lib32-opencl-icd-loader" "libxslt" "lib32-libxslt" "libva" "lib32-libva" "gtk3" "lib32-gtk3" "gst-plugins-base-libs" "lib32-gst-plugins-base-libs" "vulkan-icd-loader" "lib32-vulkan-icd-loader")
git_name="TheBotlyNoob"
git_email="thebotlynoob@gmail.com"
git_domain="github.com"
git_password=""

dir="$(dirname "$(readlink -f "$0")")"

if [ -f "$dir"/.env ]; then
  source "$dir"/.env "${packages[*]}" "${npm_packages[*]}" "${pip_packages[*]}"
fi

shopt -s globstar dotglob nullglob extglob

cecho() {
  local code="\033["
  local color
  case "$1" in
    black  | bk) color="${code}0;30m";;
    red    |  r) color="${code}1;31m";;
    green  |  g) color="${code}1;32m";;
    yellow |  y) color="${code}1;33m";;
    blue   |  b) color="${code}1;34m";;
    purple |  p) color="${code}1;35m";;
    cyan   |  c) color="${code}1;36m";;
    gray   | gr) color="${code}0;37m";;
    *) color=""
  esac
  echo -e "$color===> $2${code}0m"
}

start() {
  if [ ! -f /usr/bin/pacman ]; then
    >&2 cecho r "ERROR - Not On Arch Or Manjaro"
    exit 1
  elif [[ ! -d "$dir/apps" || ! -d "$dir/fonts" || ! -d "$dir/scripts" ]]; then
    >&2 cecho y "WARNING - Only Downloaded Install Script, Installing Extra Files" 
    sudo bash -c "mkdir -p /tmp;wget 'https://github.com/TheBotlyNoob/My-Arch-Config/archive/refs/heads/master.zip' -O /tmp/Arch-Config.zip --quiet --continue --show-progress;rm -rf /tmp/My-Arch-Config-master &>/dev/null;unzip -qq /tmp/Arch-Config.zip -d /tmp;rm /tmp/Arch-Config.zip;bash /tmp/My-Arch-Config-master/install.sh"
  elif [[ $EUID -ne 0 ]]; then
    exec sudo bash "$0" "$@"
  else 
    setup
    installPackages
    updateUsers
    updateDNS
    cleanup
  fi
}

alias cp="cp -f"

cleanup() {
  userdel tmp > /dev/null
  rm -rf /home/tmp
  trap "exit 0" SIGINT
  cecho b "Downloading The Manjaro Gnome ISO To /opt/manjaro-gnome.iso"
  bash -c "mkdir -p /opt;cd /opt;wget \"https://download.manjaro.org/gnome/21.1.1/manjaro-gnome-21.1.1-210827-linux513.iso\" -O /opt/manjaro-gnome.iso --quiet --continue --show-progress"
  cecho p "Rebooting In \033[0;31mFive Seconds"
  sleep 5
  reboot
}

setup() {
  cecho b "Updating Packages..."
  pacman --noconfirm -Syu
  pacman --noconfirm -S --needed lshw yay
  useradd -m tmp
  mkdir -p /home/tmp
  chmod 777 /home/tmp
  chown -R tmp /home/tmp
  echo "tmp ALL=(ALL) ALL" >> /etc/sudoers
  passwd -d tmp
  cecho b "Installing Extra Apps And Fonts..."
  mkdir -p /apps/ /wine/ /usr/local/share/fonts/
  cp -R "$dir"/apps/* /apps/
  cp -R "$dir"/fonts/* /usr/local/share/fonts/
  chmod 777 -R /apps/ /wine/ "$dir"
  setfont /usr/local/share/fonts/Operator/OperatorMonoLig-Book.ttf
  trap "cleanup" SIGINT
}

installPackages() {
  kernals=$(uname -r | grep -Po "([0-9]*)\.([0-9]*)" | grep -m 1 "" | tr -d ".")
  for kernal in $kernals; do deps+=("$kernal-virtualbox-host-modules"); done
  gpu=$(sudo lshw -C display | grep vendor -m 1)
  if [[ $gpu =~ "NVIDIA" ]]; then deps+=("nvidia-dkms" "nvidia-settings" "nvidia-utils" "lib32-nvidia-utils"); elif [[ $gpu =~ "AMD" ]]; then deps+=("xf86-video-amdgpu" "lib32-mesa" "vulkan-radeon" "lib32-vulkan-radeon"); elif [[ $gpu =~ "Intel" ]]; then deps+=("lib32-mesa" "vulkan-intel" "lib32-vulkan-intel"); fi
  cecho b "Installing Extra Packages..."
  sleep 2
  for package in "${packages[@]}"; do sudo -u tmp yay -S --noconfirm "$package"; done
  for package in "${npm_packages[@]}"; do /usr/bin/npm install -g "$package"; done
  for package in "${pip_packages[@]}"; do "$dir"/apps/python/bin/pip3 install "$package"; done
}

updateUsers() {
  for user in /home/!(tmp); do
    user=$(basename "$user")
    cecho b "Setting Up $user..."
    mkdir -p /home/"$user"/Desktop /home/"$user"/.config/autostart
    cp "$dir"/scripts/**/* /home/"$user"
    cp /usr/share/applications/{discord,steam}.desktop /home/"$user"/.config/autostart
    cp /usr/share/applications/{minecraft-launcher,google-chrome}.desktop /home/"$user"/Desktop
    echo "disable=SC1091" >> /home/"$user"/.shellcheckrc
    sudo -u "$user" git config --global credential.helper store
    sudo -u "$user" git config --global user.email "$git_email"
    sudo -u "$user" git config --global user.name "$git_name"
    sudo -u "$user" git config --global user.password "$git_password"
    sudo -u "$user" git config --global init.defaultBranch "master"
    echo "https://$git_name:$git_password@$git_domain" > "$user"/.git-credentials
    chsh -s /usr/bin/zsh "$user"
    mkdir -p /wine/x64-"$user" /wine/x32-"$user"
    chown "$user" /wine/x64-"$user" /wine/x32-"$user"
    sudo -u "$user" WINEARCH="win64" WINEPREFIX="/wine/x64-$user" wine wineboot
    sudo -u "$user" WINEARCH="win32" WINEPREFIX="/wine/x32-$user" wine wineboot
  done
}

updateDNS() {
  cecho b "Updating DNS Servers..."
  # https://serverfault.com/a/1064938
  nmcli -g name,type connection  show  --active | awk -F: '/ethernet|wireless/ { print $1 }' | while read -r connection; do
    nmcli con mod "$connection" ipv6.ignore-auto-dns yes
    nmcli con mod "$connection" ipv4.ignore-auto-dns yes
    nmcli con mod "$connection" ipv4.dns "${dns_servers[*]}"
    nmcli con down "$connection" && nmcli con up "$connection"
  done
}

start "$@"
exit
